import webpack from 'webpack';
import path from 'path';
import dotenv from 'dotenv';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import postcssOptions from './postcss.config';

const {InjectManifest} = require('workbox-webpack-plugin');

const env = dotenv.config();
const IS_DEVELOPMENT = process.env?.NODE_ENV === 'development';

const plugins = [
  IS_DEVELOPMENT && new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new MiniCssExtractPlugin(),
  !IS_DEVELOPMENT &&
    new InjectManifest({
      swSrc: './sw.ts',
    }),
].filter(Boolean) as webpack.WebpackPluginInstance[];

const config: webpack.Configuration = {
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },
      {
        test: /\.(css|s[ac]ss)$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
          {
            loader: 'postcss-loader',
            options: {postcssOptions},
          },
        ],
      },
      {
        test: /\.(js|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  context: path.join(process.cwd(), 'src/client'),
  devtool: 'cheap-module-source-map',
  entry: [
    'regenerator-runtime/runtime',
    IS_DEVELOPMENT && 'webpack-hot-middleware/client',
    IS_DEVELOPMENT && 'react-hot-loader/patch',
    !IS_DEVELOPMENT && 'sw.ts',
    'index',
  ].filter(Boolean) as string[],
  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.json', 'css', 'scss', 'sass'],
    modules: [
      path.join(process.cwd(), 'node_modules'),
      path.join(process.cwd(), 'src/client'),
    ],
    alias: {
      'react-dom': '@hot-loader/react-dom',
      '@components': path.resolve(process.cwd(), 'src/client/components/'),
      '@typings': path.resolve(process.cwd(), 'typings'),
      '@actions': path.resolve(process.cwd(), 'src/client/store/actions/'),
      '@pages': path.resolve(process.cwd(), 'src/client/pages/'),
      '@queries': path.resolve(process.cwd(), 'src/client/queries/'),
    },
  },
  output: {
    publicPath: '/',
  },
  plugins,
  mode: (process.env.NODE_ENV as any) || 'production',
};

export default config;
