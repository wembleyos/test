/* eslint-disable global-require */
import {TailwindConfig} from 'tailwindcss/tailwind-config';
import tailwindConfig from './tailwind.config';

// eslint-disable-next-line import/no-extraneous-dependencies
const tailwindcss = require('tailwindcss');

const postcssConfig = {
  plugins: [
    require('autoprefixer'),
    tailwindcss(tailwindConfig as unknown as TailwindConfig),
  ],
};

export default postcssConfig;
