/* eslint-disable import/no-extraneous-dependencies */
import path from 'path';
import dotenv from 'dotenv';

const defaultConfig = require('tailwindcss/defaultConfig');
const defaultTheme = require('tailwindcss/defaultTheme');

const defaultColors = require('tailwindcss/colors');

const env = dotenv.config().parsed;

const colors = {
  ...defaultColors,
};

const theme = {
  ...defaultTheme,
  colors,
  container: {
    center: true,
    padding: '2rem',
  },
  screens: {
    sm: '640px',
    md: '768px',
    lg: '1024px',
    xl: '1280px',
  },
};

const variants = {
  extend: {
    backgroundColor: ['hover', 'even', 'odd'],
  },
};
const purge = env?.NODE_ENV !== 'development' && {
  content: [
    path.join(process.cwd(), 'src/client/**/*.tsx'),
    path.join(process.cwd(), 'src/client/**/*.ts'),
    path.join(process.cwd(), 'src/client/**/*.scss'),
    path.join(process.cwd(), 'src/client/**/*.sass'),
    path.join(process.cwd(), 'src/client/**/*.css'),
  ],
};

const tailwindConfig = {
  ...defaultConfig,
  theme,
  variants,
  purge,
};

export default tailwindConfig;
