FROM node:alpine

RUN apk add --no-cache make gcc g++ python3 && \
  apk del make gcc g++ python3

RUN mkdir -p /home/node/app/node_modules
WORKDIR /home/node/app
ENV PATH /home/node/app/node_modules/.bin:$PATH

COPY package*.json ./

RUN yarn install

EXPOSE 3000

CMD [ "yarn", "prod" ]
