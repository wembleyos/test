import {ConfigActionTypes, configState} from './ConfigState';

export type ActionTypes = ConfigActionTypes;

export interface Store {
  dispatch?: React.Dispatch<ActionTypes>;
  config: typeof configState;
}

export const rootState: Store = {
  config: configState,
};
