export interface AlbumTrack {
  id: string;
  position: number;
  title: string;
}

interface AlbumMedia {
  'status-id': string;
  format: string;
  'track-count': number;
  title: string;
  tracks: AlbumTrack[];
}

export interface Album {
  status: string;
  country: string;
  id: string;
  media: AlbumMedia[];
  quality: string;
  title: string;
}
