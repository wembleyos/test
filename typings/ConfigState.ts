import setLanguage from '@actions/setLanguage';
import {Languages} from './Language';

export enum ConfigActions {
  SET_LANGUAGE = 'CONFIG/SET_LANGUAGE',
}

export type ConfigActionTypes = ReturnType<typeof setLanguage>;

interface ConfigState {
  language: Languages;
}

export const configState: ConfigState = {
  language: 'en',
};
