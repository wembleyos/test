import 'reflect-metadata';

import dotenv from 'dotenv';
import express from 'express';

import path from 'path';
import {devMiddleware, hotMiddleware, htmlRenderer} from './middleware';

require('@babel/register')();

dotenv.config();
const app = express();

app.use(express.static(path.join(process.cwd(), 'public')));
app.use(express.static(path.join(process.cwd(), 'dist')));

app.use(devMiddleware);

if (process.env.NODE_ENV === 'development') {
  app.use(hotMiddleware);
}

devMiddleware.waitUntilValid(() => {
  app.get('/*', htmlRenderer);
  app.listen(3000, () =>
    process.stdout.write('\nApp listening on http://localhost:3000\n'),
  );
});
