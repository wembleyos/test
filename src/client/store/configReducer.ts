import {configState, ConfigActions, ConfigActionTypes} from '@typings';

const configReducer: React.Reducer<typeof configState, ConfigActionTypes> = (
  state,
  action,
) => {
  switch (action.type) {
    case ConfigActions.SET_LANGUAGE: {
      return {
        ...state,
        language: action.language,
      };
    }
    default:
      return state;
  }
};

export default configReducer;
