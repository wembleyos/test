import {ActionTypes, Store} from '@typings';
import configReducer from './configReducer';

const mainReducer = ({config}: Store, action: ActionTypes) => ({
  config: configReducer(config, action),
});

export default mainReducer;
