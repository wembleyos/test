import {ConfigActions, Languages} from '@typings';

interface SetLanguageAction {
  type: ConfigActions.SET_LANGUAGE;
  language: Languages;
}

const setLanguage = (language: Languages): SetLanguageAction => ({
  type: ConfigActions.SET_LANGUAGE,
  language,
});

export default setLanguage;
