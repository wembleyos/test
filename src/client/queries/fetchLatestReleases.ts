import {Album} from '@typings';
import axios from 'axios';

const fetchLatestReleases = async () => {
  const {data} = await axios.get<{releases: Album[]}>(
    'https://musicbrainz.org/ws/2/release/?artist=0383dadf-2a4e-4d10-a46a-e9e041da8eb3&inc=recordings&fmt=json',
  );
  return data;
};

export default fetchLatestReleases;
