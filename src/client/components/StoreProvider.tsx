import {rootState} from '@typings';
import React, {useContext, useReducer} from 'react';
import mainReducer from '../store';

const Context = React.createContext(rootState);

interface StoreProviderProps {
  children: React.ReactElement;
}

const StoreProvider: React.FC<StoreProviderProps> = ({children}) => {
  const [state, dispatch] = useReducer(mainReducer, rootState);

  return (
    <Context.Provider value={{...state, dispatch}}>{children}</Context.Provider>
  );
};

export const useStore = () => useContext(Context);
export default StoreProvider;
