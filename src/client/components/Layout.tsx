import setLanguage from '@actions/setLanguage';
import React from 'react';
import {useStore} from './StoreProvider';

interface LayoutProps {
  children: React.ReactElement;
}

const Layout: React.FC<LayoutProps> = ({children}) => {
  const {dispatch, config} = useStore();

  const targetLanguage = config.language === 'pl' ? 'en' : 'pl';
  return (
    <>
      <header className="fixed top-0 left-0 w-full h-12 border-b flex bg-white z-10">
        <div className="container relative h-full flex items-center">
          <button
            type="button"
            className="px-4 cursor-pointer"
            onClick={() => dispatch(setLanguage(targetLanguage))}>
            {targetLanguage}
          </button>
        </div>
      </header>
      <main className="container mt-12">{children}</main>
    </>
  );
};

export default Layout;
