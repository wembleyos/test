import React from 'react';
import {Album, AlbumTrack} from '@typings';

interface AlbumListItemProps {
  data: Album;
  onDelete: (track: AlbumTrack) => void;
  onSelect: (track: AlbumTrack) => void;
  deleted: string[];
  selected: string[];
}

const AlbumListItem: React.FC<AlbumListItemProps> = ({
  data,
  onDelete,
  onSelect,
  deleted,
  selected,
}) => (
  <div className="border-b border-l border-r hover:shadow-sm p-4">
    <div className="flex flex-row flex-wrap mb-2 py-4">
      {data.title} ({data.country})
    </div>
    <div className="text-sm ml-8">
      {data.media?.map((media) => (
        <div key={media['status-id']}>
          {media.tracks
            .filter((track) => !deleted.includes(track.id))
            .map((track) => (
              <div
                key={track.id}
                className="py-2 border-b flex flex-row flex-wrap items-center justify-between  odd:bg-gray-100 px-4">
                <p>
                  {track.position} {track.title}
                </p>
                <div className="text-xs flex flex-row flex-wrap items-center">
                  {selected?.includes(track.id) && (
                    <div className="text-green-600 mr-4">Best of best</div>
                  )}
                  {!selected?.includes(track.id) && (
                    <button
                      type="button"
                      className="px-4 py-2 mr-4 border bg-green-400 hover:bg-green-900 rounded text-white transition ease-in-out"
                      onClick={() => onSelect(track)}>
                      Mark as best
                    </button>
                  )}
                  <button
                    type="button"
                    className="px-4 py-2 border bg-red-400 hover:bg-red-700 rounded text-white transition ease-in-out"
                    onClick={() => onDelete(track)}>
                    Delete
                  </button>
                </div>
              </div>
            ))}
        </div>
      ))}
    </div>
  </div>
);

export default AlbumListItem;
