import {Album} from '@typings';
import React, {useEffect, useState} from 'react';
import AlbumListItem from './AlbumListItem';

interface AlbumListProps {
  collection: Album[];
}

const AlbumList: React.FC<AlbumListProps> = ({collection}) => {
  const [deletedTracks, setDeletedTracks] = useState<string[]>([]);
  const [bestTracks, setBestTracks] = useState<string[]>([]);

  useEffect(() => {
    if (typeof window === 'undefined') {
      return;
    }
    if (deletedTracks?.length) {
      localStorage.setItem('deletedTracks', JSON.stringify(deletedTracks));
    }

    if (bestTracks?.length) {
      localStorage.setItem('bestTracks', JSON.stringify(bestTracks));
    }
  }, [deletedTracks, bestTracks]);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      setDeletedTracks(
        (JSON.parse(localStorage.getItem('deletedTracks')) as string[]) || [],
      );
      setBestTracks(
        (JSON.parse(localStorage.getItem('bestTracks')) as string[]) || [],
      );
    }
  }, []);

  return (
    <div className="my-8">
      {collection?.map((album) => (
        <AlbumListItem
          data={album}
          key={album.id}
          deleted={deletedTracks}
          selected={bestTracks}
          onSelect={(track) => setBestTracks(bestTracks.concat(track.id))}
          onDelete={(track) => setDeletedTracks(deletedTracks.concat(track.id))}
        />
      ))}
    </div>
  );
};

export default AlbumList;
