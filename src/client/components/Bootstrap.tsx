import setLanguage from '@actions/setLanguage';
import {Languages} from '@typings';
import React, {useEffect} from 'react';
import {Helmet} from 'react-helmet';
import Layout from './Layout';
import {useStore} from './StoreProvider';

interface BootstrapProps {
  children: React.ReactElement;
}

const Bootstrap: React.FC<BootstrapProps> = ({children}) => {
  const {dispatch, config} = useStore();

  useEffect(() => {
    if (typeof window !== 'undefined') {
      dispatch(setLanguage(navigator.language.split('-')[0] as Languages));
    }
  }, []);

  return (
    <>
      <Helmet>
        <html lang={config.language} />
      </Helmet>
      <Layout>{children}</Layout>
    </>
  );
};

export default Bootstrap;
