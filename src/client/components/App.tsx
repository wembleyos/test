import React from 'react';
import {hot} from 'react-hot-loader';

import {renderRoutes} from 'react-router-config';
import routes from '../routes';

const App: React.FC = () => <div>{renderRoutes(routes)}</div>;

export default hot(module)(App);
