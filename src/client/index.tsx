import React from 'react';
import {render} from 'react-dom';
import {renderRoutes} from 'react-router-config';
import {BrowserRouter} from 'react-router-dom';

import 'styles/index.scss';
import {QueryClient, QueryClientProvider} from 'react-query';
import {Hydrate} from 'react-query/hydration';
import routes from './routes';

// eslint-disable-next-line no-underscore-dangle
const dehydratedState = (window as any).__REACT_QUERY_STATE__;

const queryClient = new QueryClient();

document.addEventListener('DOMContentLoaded', () => {
  render(
    <QueryClientProvider client={queryClient}>
      <Hydrate state={dehydratedState}>
        <BrowserRouter>{renderRoutes(routes)}</BrowserRouter>
      </Hydrate>
    </QueryClientProvider>,
    document.querySelector('#app'),
  );
});
