import AlbumList from '@components/Album/AlbumList';
import fetchLatestReleases from '@queries/fetchLatestReleases';
import React from 'react';
import {hot} from 'react-hot-loader';
import {useQuery} from 'react-query';

const HomePage: React.FC = () => {
  const {data} = useQuery('latestReleases', fetchLatestReleases);

  return (
    <div className="md:w-1/2 mx-auto">
      <AlbumList collection={data?.releases} />
    </div>
  );
};

export default hot(module)(HomePage);
