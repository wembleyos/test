import Root from '@components/Root';
import HomePage from '@pages/HomePage';
import fetchLatestReleases from '@queries/fetchLatestReleases';
import {QueryClient} from 'react-query';
import {match} from 'react-router';
import {RouteConfig} from 'react-router-config';

const routes: RouteConfig[] = [
  {
    component: Root,
    routes: [
      {
        path: '/',
        component: HomePage,
        loadData: async (routeMatch: match, queryClient: QueryClient) => {
          await queryClient.prefetchQuery(
            'latestReleases',
            fetchLatestReleases,
          );
        },
      },
    ],
  },
];

export default routes;
