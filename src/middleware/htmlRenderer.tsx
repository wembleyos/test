import React from 'react';
import {query, Request, Response} from 'express';
import isObject from 'lodash/isObject';
import {renderToString} from 'react-dom/server';
import {StaticContext, StaticRouter} from 'react-router';
import routes from 'src/client/routes';
import Helmet from 'react-helmet';
import {matchRoutes, renderRoutes} from 'react-router-config';

import {QueryClient, QueryClientProvider} from 'react-query';
import {dehydrate, Hydrate} from 'react-query/hydration';

function normalizeAssets(assets: Record<string, string> | []) {
  if (isObject(assets)) {
    return Object.values(assets);
  }

  return Array.isArray(assets) ? assets : [assets];
}

const loadBranchData = (location: string, queryClient: QueryClient) => {
  const branch = matchRoutes(routes, location);

  const promises = branch.map(({route, match}) =>
    route.loadData ? route.loadData(match, queryClient) : Promise.resolve(null),
  );

  return Promise.all(promises);
};

const htmlRenderer = async (req: Request, res: Response) => {
  const {devMiddleware} = res.locals.webpack;
  const jsonWebpackStats = devMiddleware.stats.toJson();
  const {assetsByChunkName} = jsonWebpackStats;
  const context: StaticContext & {url?: string} = {};

  const queryClient = new QueryClient();
  await loadBranchData(req.path, queryClient);
  const dehydratedState = dehydrate(queryClient);

  const jsx = renderToString(
    <QueryClientProvider client={queryClient}>
      <Hydrate state={dehydratedState}>
        <StaticRouter context={context} location={req.path}>
          {renderRoutes(routes)}
        </StaticRouter>
      </Hydrate>
    </QueryClientProvider>,
  );
  const helmet = Helmet.renderStatic();

  if (context.url) {
    // can use the `context.status` that
    // we added in RedirectWithStatus
    res.redirect(context.statusCode || 200, context.url);
  }

  return res.send(`<!DOCTYPE html>
    <html ${helmet.htmlAttributes.toString() || ''}>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${normalizeAssets(assetsByChunkName.main)
          .filter((file) => file.endsWith('.css'))
          .map(
            (file) => `<link rel="stylesheet" href="/${file}" media="all" />`,
          )
          .join('\n')}
    </head>
    <body ${helmet.bodyAttributes.toString() || ''}>
    <div id="app">${jsx}</div>
    ${normalizeAssets(assetsByChunkName.main)
      .filter((file: string) => file.endsWith('.js'))
      .filter((file: string) => !file.includes('hot'))
      .map((file: string) => `<script src="/${file}"></script>`)
      .join('\n')}
    <script>
      window.__REACT_QUERY_STATE__ = ${JSON.stringify(dehydratedState)};
    </script>
    </body>
    </html>`);
};

export default htmlRenderer;
