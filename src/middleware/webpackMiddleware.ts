import webpack from 'webpack';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import WebpackHotMiddleware from 'webpack-hot-middleware';

import webpackConfig from '../../config/webpack.config';

const compiler = webpack(webpackConfig);

export const devMiddleware = WebpackDevMiddleware(compiler, {
  serverSideRender: true,
  stats: 'errors-warnings',
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const hotMiddleware = WebpackHotMiddleware(compiler as any, {});

export default {
  devMiddleware,
  hotMiddleware,
};
